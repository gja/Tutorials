{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\n",
    "## 10: Model Fitting: \n",
    "\n",
    "These problems are provided as Jupyter notebooks, so that you can edit your own version and include some simple calculations. You don't have to - you can do your own calculations any way you like - but it will get you used to doing statistics calculations with Python, which will gradually be more powerful in later weeks.\n",
    "\n",
    "\n",
    "This is the version with solutions.\n",
    "\n",
    "**Python setup**\n",
    "\n",
    "Once again, lets start by loading up some standard Python modules."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "from math import *  # basic maths routines\n",
    "import numpy as np  # more advanced maths routines\n",
    "from scipy import stats # stats routines, e.g. binomial and Poisson distributions"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Level 1 Problems: simple calculations\n",
    "These are fairly simple calculations, the problems are mainly to test understanding of concepts and definitions."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "solution2": "hidden",
    "solution2_first": true
   },
   "source": [
    "### P10.1 Confidence range from the covariance matrix (L1)\n",
    "A model involves two parameters $p$ and $q$. Priors have been assigned, experimental data obtained, and the posterior probability surface calculated.  The peak of the posterior probability surface is found to be at $p=19.7$, $q=23.4$. \n",
    "A Gaussian approximation to the surface gives the covariance matrix\n",
    "\n",
    "$$\n",
    "\\left(\n",
    "\\begin{array}{cc}\n",
    "9.7 & 0.0 \\\\\n",
    "0.0 & 16.3\n",
    "\\end{array}\n",
    "\\right)\n",
    "$$\n",
    "\n",
    "Are the parameters correlated? What is the difference between conditinal and joint error ranges.\n",
    "What is the 90% conditional error range on $p$? What is the 90% joint error range on $p$?\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "solution2": "hidden"
   },
   "source": [
    "**Solution** The off-diagonal elements give the value of the covariance, $\\sigma_{ab}$ which is zero, so the parameters are not correlated.\n",
    "\n",
    "The 90% conditional error is the range of $p$ for a fixed $q$, essentially an integral of a 1D Gaussian up to a limit  (in units of $\\sigma$) which encloses 90% of the probability.\n",
    "The 90% joint error is the range of $p$ and $q$, essentially an integral of a 2D Gaussian up to a limit  (in units of $\\sigma$) which encloses 90% of the probability.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "solution2": "hidden"
   },
   "source": [
    "The top-left element of the matrix is $\\sigma_p^2$   \n",
    "So the conditional error is $\\sigma_p=3.11$   \n",
    "To get 90% we need to go $1.64\\sigma$   \n",
    "So the 90% range is $p=19.7 \\pm 5.1 = 14.6 - 24.8$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "solution2": "hidden"
   },
   "source": [
    "For the joint error, with two parameters, we need to go to $k\\sigma$ where $k=1.52$.  \n",
    "For 90% we need $k=1.52\\times 1.64=2.15$   \n",
    "So the joint error range is $p=19.7\\pm 6.69 = 13.01 - 26.39$ \n",
    "\n",
    "Note $\\sigma_q$ is irrelevant because the parameters are not correlated."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Level 2 Problems: formulating problems\n",
    "These are more like real-world problems. The key skill is being able to translate a physical description into appropriate mathematics, and then identifying the concepts and formulae you need to apply. Sometimes at that point its a straightforward calculation, but more often you need a bit more mathematical manipulation, keeping the physical meaning in mind, and then finally calculating.   "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "solution2": "hidden",
    "solution2_first": true
   },
   "source": [
    "### P10.2 Eyeballing the joint error range (L2)\n",
    "A model involves two parameters $a$ and $b$. Analysis of experimental measurements gives a posterior probability surface which is a bivariate gaussian for which the $1-\\sigma$ contour is shown in the figure below. By eye, estimate the joint error: i.e. the range of $a$ which contains 68\\% of the probability density.\n",
    "<figure style=\"text-align: center\">\n",
    "<br>\n",
    "<img src=\"2D-err-soln.jpg\" alt=\"hmm\" width=\"500\">\n",
    "<br>\n",
    "</figure>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "solution2": "hidden"
   },
   "source": [
    "**Solution** The parameters are clearly correlated. We therefore need the bounding box for the contour. This is sketched in the figure below."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "solution2": "hidden"
   },
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "solution2": "hidden"
   },
   "source": [
    "The annotation gives an estimate for best fit $a=464$, range $\\pm192$\n",
    "\n",
    "But we are in 2D so to have the usual 68\\% of the probability density inside the error bars we need $k=1.52$, so we have $\\pm 292$\n",
    "\n",
    "So we have $a=464\\pm 292 = 172 - 756$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### P10.3 Fit quality and error size (L2)\n",
    "We reconsider the correlation between maths, physics, and art test marks obtained by a sample of schoolchildren. (The table is reproduced below.) When regressing the physics marks on the maths mark, a fit was found of the form $y=a+bx$ with $a=4.276$ and $b=0.658$. This is the best fit, but is this best fit any good? For this we would need to know the typical data point error. Taking the maths mark as the independent variable, and the physics mark as the dependent variable, and taking the typical error on the physics marks to be $\\sigma=5$, calculate the $\\chi^2$ for this fit. Suppose instead $\\sigma=3.5$. Re-calculate the $\\chi^2$. What is the quality of the fit in these two cases? What general lesson does this hold?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\n",
    "| student | maths mark | physics mark | art mark |\n",
    "|---------|------------|--------------|----------|\n",
    "| A       | 41         | 36           | 38       |\n",
    "| B       | 37         | 20           | 44       |\n",
    "| C       | 38         | 31           | 35       |\n",
    "| D       | 29         | 24           | 49       |\n",
    "| E       | 49         | 37           | 35       |\n",
    "| F       | 47         | 35           | 29       |\n",
    "| G       | 42         | 42           | 42       |\n",
    "| H       | 34         | 26           | 36       |\n",
    "| I       | 36         | 27           | 32       |\n",
    "| J       | 48         | 29           | 29       |\n",
    "| K       | 29         | 23           | 22       |\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "solution2": "hidden",
    "solution2_first": true
   },
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "solution2": "hidden"
   },
   "source": [
    "**Solution** If $M$ is the maths mark, then we take this as the independent variable, and then predict the physics marks as $P_i(pred) = a + bM_i$ with $a=4.276$ and $b=0.658$. Then for each student we do the deviation $P_i-P_i(pred)$, and do the sum of squares divided by $\\sigma^2$\n",
    "\n",
    "We start by assuming $\\sigma=5$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "solution2": "hidden"
   },
   "outputs": [],
   "source": [
    "#observed data\n",
    "M=[41,37,38,29,49,47,42,34,36,48,29]\n",
    "P=[36,20,31,24,37,35,42,26,27,29,23]\n",
    "N=len(M)\n",
    "\n",
    "# model\n",
    "a=4.276\n",
    "b=0.658\n",
    "\n",
    "# make the predictions and calculate chisq\n",
    "sum=0.0\n",
    "Ppred=np.zeros(N)\n",
    "for i in range(0,N):\n",
    "    Ppred[i]=a+b*M[i]\n",
    "    sum=sum+ (P[i]-Ppred[i])**2\n",
    "    \n",
    "sigma1=5\n",
    "chisq1=sum/(sigma1**2)\n",
    "sigma2=3.5\n",
    "chisq2=sum/(sigma2**2)\n",
    "print(\"Assuming sigma=\",sigma1, \"gives chisq=\", round(chisq1,2))\n",
    "print(\"Assuming sigma=\",sigma2, \"gives chisq=\", round(chisq2,2))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "solution2": "hidden"
   },
   "source": [
    "Next we ask, what is the probability of exceeding that $\\chi^2$?, given the degrees of freedom is $\\nu=N-2$?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "solution2": "hidden"
   },
   "outputs": [],
   "source": [
    "nu=N-2; print(N,nu)\n",
    "P1=stats.chi2.sf(chisq1,nu)\n",
    "P2=stats.chi2.sf(chisq2,nu)\n",
    "\n",
    "print(\"Assuming sigma=\",sigma1, \"gives P>chisq=\", round(P1*100,1),\"%\")\n",
    "print(\"Assuming sigma=\",sigma2, \"gives P>chisq=\", round(P2*100,1),\"%\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "solution2": "hidden"
   },
   "source": [
    "If the typical error is indeed 5, we would accept that model. If instead the error is 3.5, we would reject the model at 95% confidence, and almost at 99% confidence. The general lesson is that one must get the errors right - as well as testing whether we have the right shape, the $\\chi^2$ test is very sensitive to the assumed error size."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "solution2": "hidden",
    "solution2_first": true
   },
   "source": [
    "### P10.4 X-ray spectrum model comparison (L2)\n",
    "An astronomer measures the X-ray spectrum of a quasar in 9 energy channels and fits the data with a simple power law model, so that there are two fitting parameters - the power law slope, and the normalisation - and gets a fit quality of $\\chi^2=11.6$. A rival astronomer fits the same data with a more sophisticated model with three fitting parameters - the power law slope, the normalisation, and an \"absorbing column density\" parameter. This model gives $\\chi^2=6.2$. Can the second astronomer definitely conclude that the absorbing column density is a useful parameter?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "solution2": "hidden"
   },
   "source": [
    "**Solution**   \n",
    "Model-1 has $\\chi^2=11.6$ and $\\nu=9-2=7$ degrees of freedom.   \n",
    "Model-2 has $\\chi^2=6.2$ and $\\nu=9-3=6$ degrees of freedom.   \n",
    "Lets compare their fit qualities"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "solution2": "hidden"
   },
   "outputs": [],
   "source": [
    "N=9\n",
    "chisq1=11.6; chisq2=6.2\n",
    "m1=2; m2=3  ## number of parameters\n",
    "nu1=N-m1; nu2=N-m2 ## degrees of freedom\n",
    "\n",
    "P1= stats.chi2.sf(chisq1,nu1)\n",
    "P2= stats.chi2.sf(chisq2,nu2)\n",
    "print(\"model-1 has P>chisq =\", round(P1*100,1),\"%\")\n",
    "print(\"model-2 has P>chisq =\", round(P2*100,1),\"%\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "solution2": "hidden"
   },
   "source": [
    "So Model-1 looks poor, but you wouldn't reject it. Model-2 looks better, but is it a significant improvement? We assess this with the $\\Delta\\chi^2$ test.\n",
    "\n",
    "We have $\\Delta\\chi^2=11.6-6.2=5.4$. This should be distributed like $\\chi^2$ with $7-6=1$ degrees of freedom."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "solution2": "hidden"
   },
   "outputs": [],
   "source": [
    "dchisq=chisq1-chisq2\n",
    "dnu=nu1-nu2\n",
    "Pdchisq = stats.chi2.sf(dchisq,dnu)\n",
    "print(\"P>delta-chisq=\",round(Pdchisq*100,1),\"%\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "solution2": "hidden"
   },
   "source": [
    "The null hypothesis is that there is no difference between the models. The probability of getting the observed $\\Delta\\chi^2$ on that assumption or larger is only 2%, so we can reject the null hypothesis at better then 95% confidence, although not at better than 99% confidence. It is interesting that although we **don't** have evidence that would allow us to reject Model-1 in isolation, we **do** have evidence that it is signifcantly worse than Model-2."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "solution2": "hidden"
   },
   "source": [
    "**Note**: It is also possible to compare the models using the $F$-test. If you try that, it also finds the second model is better, but the significance is much less. This may seem confusing at first, but there is no reason that the two tests should give the same answer - they are mathematically different tests, asking different questions. It can be tempting to try different tests until you get a significant result... but then you are falling into the multiple tests trap... your result is not as significant as you think...."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Level 3 Problems: needs some thought\n",
    "\n",
    "These problems are not so straightforward, needing a little more careful thought or imagination. Sometimes the trick or insight is quite simple, and sometimes its quite hard! Often, these problems force you to think more carefully about the fundamentals, or question your assumptions, to avoid falling into traps."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "solution2": "hidden",
    "solution2_first": true
   },
   "source": [
    "### P10.5 Errors on four parameter fit (L3)\n",
    "For a four-parameter fit, what multiple of the conditional error gives the full joint error? What $\\Delta\\chi^2$ does this correspond to?\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "solution2": "hidden"
   },
   "source": [
    "**Solution** It is easiest to answer this the other way round.\n",
    "\n",
    "For $m=4$ parameters, $\\Delta\\chi^2$ will be distributed as $\\chi_4^2$, i.e. $\\chi^2$ with $\\nu=4$. Looking up in a $\\chi^2$ table, we find $\\Delta\\chi^2=4.72$\n",
    "\n",
    "The multiple of $\\sigma$ is given by $k=\\sqrt{\\Delta\\chi^2}=2.17$\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.9"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
